// 首先引入我们自定义的axios对象
import request from '@/utils/request'

// 将前缀抽取出来，以后改的话就改这个地方就行

// 导出对象
export default {
    getList(){
        // 把请求对象返回，提供给组件使用
        const req = request({
            methods: 'get',
            url: '/db.json'
        })
        return req
    }
}