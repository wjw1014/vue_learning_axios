import Vue from "vue";
import App from "./App.vue";
import router from "./router";


Vue.config.productionTip = process.env.NODE_ENV === 'production';
console.log(process.env.NODE_ENV)
console.log('代理地址 --->', process.env.VUE_APP_WJW_URL);  // 开发环境 development ，生产环境 production

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
